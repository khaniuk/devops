# Permissions

List permissions, firs char file or directory, next permission, owner group and user

- r = read is iqual to number 4
- w = write is iqual to number 2
- x = execute is equal to numner 1
- rwx = total sum 7

```
ls -l       //If firts char is - (file), d (directory)
```

Change permission, parameter **-R** is recurvsive. Is necessary user root

```
chmod 766 test.txt       //example for file

chmod -R 766 test/       //example for directory

chmod -rwx file          //remove permission
chmod +rwx file          //add permission
```

| permission  |    owner    |
|-------------|-------------|
|  rwx------  | user        |
|  ---r-x---  | group       |
|  ------r-x  | other       |

Remove permission to ***group*** and ***other***

```
chmod g-x,o-x file
```

**chmod** in octal

| Perm | Num | Description                       |
|------|-----|-----------------------------------|
| rwx  |  7  | Lectura, escritura y ejecución    |
| rw-  |  6  | Lectura, escritura                |
| r-x  |  5  | Lectura y ejecución               |
| r--  |  4  | Lectura                           |
| -wx  |  3  | Escritura y ejecución             |
| -w-  |  2  | Escritura                         |
| --x  |  1  | Ejecución                         |
| ---  |  0  | Sin permisos                      |

Result for apply

|       Permission       | Octal use |
|------------------------|-----------|
|chmod u=rwx,g=rwx,o=rx  | chmod 775 | 
|chmod u=rwx,g=rx,o=     | chmod 760 |
|chmod u=rw,g=r,o=r      | chmod 644 |
|chmod u=rw,g=r,o=       | chmod 640 |
|chmod u=rw,go=          | chmod 600 |
|chmod u=rwx,go=         | chmod 700 |

## Permission for web application

```
sudo find /var/www/html/project -type d -exec chmod 0755 {} \;
sudo find /var/www/html/project -type f -exec chmod 0644 {} \;
```