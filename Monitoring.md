# Monitoring

Command `top` show ***CPUs***, ***RAM***, Run process PID and others information

```
top     //q for exit

htop    //similary to top
```

Command `fee` show information of ***RAM***

```
free -mto
```

Command `df` show information of ***Hard Disc*** and parameter `-h` for show in GB

```
df -h
```

Command `w` show information of ***users*** connected

```
w
```

Command `date` display current ***date*** of system

```
date
```