# SSH

Connect to server

```
ssh user@192.168.20.5

ssh user@192.168.20.5 22    //Specific port number
```

Upload file ***index.html***

```
scp /home/user/index.html user@30.30.30.31:/var/www/html/
```

Upload directory ***project*** with parameter `-r` (recursive)

```
scp -r /home/user/project user@30.30.30.31:/var/www/html/
```

Download file ***index.html***

```
scp user@30.30.30.31:/var/www/html/index.html /home/user/
```