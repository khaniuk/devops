# DevOps

Commands list for Linux
_________________________________________________
## Linux Debian and Centos

Index

* [Monitoring](https://gitlab.com/khaniuk/devops/-/blob/master/Monitoring.md)
* [User & Group](https://gitlab.com/khaniuk/devops/-/blob/master/UserGroup.md)
* [Permission](https://gitlab.com/khaniuk/devops/-/blob/master/Permission.md)
* [Network & Hostname](https://gitlab.com/khaniuk/devops/-/blob/master/NetworkHostname.md)
* [SSH](https://gitlab.com/khaniuk/devops/-/blob/master/SSH.md)
* [Backup](https://gitlab.com/khaniuk/devops/-/blob/master/Backup.md)
* [NFS](https://gitlab.com/khaniuk/devops/-/blob/master/NFS.md)

### Basic Commands

Use help for commands `command_name --help` for example `ls --help`

Navigating the File System (Changing directory)

```
cd /location/direcrtoty

cd ..       //go back one level
```

Current Working Directory

```
pwd
```

Listing directory contents

```
ls

ls /usr

ls -l /etc/hosts
```

Displaying file contents

```
cat /etc/hosts
```

Creating files

```
touch file.txt

cat > file.txt
```

Creating directories

```
mkdir /tmp/newdirectory

mkdir -p projects/web/app
```

Creating symbolic links

```
ln -s source_file symbolic_link
```

Remove a file or directory
```
rm file.txt

rm -i file.txt          //rm: remove regular empty file 'file.txt'?

rm -d dirname           //remove one or more empty directories

rm -rf dirname          //emove non-empty directories and all the files within them recursively
```

Copying files and directories

```
cp file file_backup

cp file.txt /backup         //copy a file to another directory

cp -R Pictures /opt/backup  //copy a directory, including all its files and subdirectories
```

Moving and renaming files and directories

```
mv file.txt /tmp

mv file.txt file1.txt           //rename a file

mv file.tx1 file1.txt /tmp      //move multiple files and directories at once, specify the destination directory
```

Find especific file

```
find . -name notes.txt

find / -type d -name notes.txt

grep blue notes.txt             //blue is text seach
```

Display first and last lines in a files

```
head -n 5 file.ext

tail -n file.ext
```


## Installing and Removing Packages

### Ubuntu and Debian

```
apt update          //update the APT package

apt upgrade         //installed packages to their latest versions

apt install package_name    //install a package

apt remove package_name     //remove a package
```

### Centos and Fedora

To install a new package on Red Hat based distributions, you can use either `yum` or `dnf` commands.

```
dnf update          //installed packages to their latest versions

dnf install package_name    //install a package

dnf remove package_name     //remove a package
```
