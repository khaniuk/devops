# Backup

Backup of directory ***/home***

Parameters

* `c` Create a file tar
* `x` Extract content from file
* `v` Show run in display.
* `z` Compress or Uncompress a file

```
tar cf backup.tar /home
```

Backup of directory ***/home*** compressed

```
tar czf backup.tar.gz /home
```

Backup all system ***/*** but is necessary exclude file backup location

```
tar cvzf backup.tar.gz /

tar czf backup.tar.gz / -exclude /mnt
```

## Divider file

Divider backup file in GB

```
split -b 1g backup.tar.gz   //1g = 1GB  or 2g = 2GB

split -b 100m backup.tar.gz   //100m = 100MB  or 200m = 200MB
```

## Concat files divided

Concat file1, file2 and fileN

```
cat file1 file2 file3 > backup.tar.gz
```
