# Let's Encrypt

Centos: Enable access to the EPEL repository on your server by typing:
```
yum install epel-release
```

## NGINX + SSL
Install
```
yum install certbot certbot-nginx mod_ssl -y	//Centos

apt install certbot certbot-nginx mod_ssl -y	//Ubuntu
```

## APACHE + SSL

Install
```
yum install certbot certbot-apache mod_ssl -y	//Centos

apt install certbot certbot-apache mod_ssl -y	//Ubuntu
```

## Run Certbot
SSL on domains detected
```
certbot --nginx

certbot --apache
```

SSL on specific domain
```
certbot --nginx -d domain.com

certbot --apache -d domain.com
```

SSL on multiple domain
```
certbot --nginx -d example.com -d www.example.com

certbot --apache -d example.com -d www.example.com
```
## Test renew

```
certbot renew --dry-run
```
