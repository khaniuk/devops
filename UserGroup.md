# User and Group

## Management User

### Create new user

Add new user to host

- -c //comment

- -g //group asigned (optional)

- -d //directory acceced, for example /home

- -m -s /bin/bash //shell access

- id or indicator of the user, unique code in OS Linux

### Change password

```
sudo passwd root        //Change password root

passwd myuser         //Change pass to other user

shutdown -r             //Reboot the system
```

### Delete user

```
userdel myuser

userdel -r username     //emove the user’s home directory and mail spool
```

## Management Group

### Create a group

```
groupadd mygroup
```

### Delete a group

```
groupdel mygroup
```

## Management User and Group

Adding users to groups

```
usermod -a -G sudo myuser

useradd -c 'myuser' -g 10 -d /home/indicador -m -s /bin/bash indicador
```

Help for commands **chmod --help**, **chown --help** and **chgrp --help**

Change user and group

```
chown user:group file

chown user:group directory

chown -R user:group directory   //apply a recursive
```

Change user only

```
chown new_user test.txt
```

Change group only

```
chgrp new_group test.txt
```

### List groups

```
sudo cat /etc/group
```