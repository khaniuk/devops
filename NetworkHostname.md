# Network & Hostname

## Network

Check my IP

```
ip a            //new command IP route 2

ifconfig        //old command
```

Test ping to IP or Domain, more trace route (traking)

```
ping 192.168.20.5       //to IP

ping test.com           //to Domain

traceroute 192.168.20.5
```

Check connection other server or chack open port

```
telnet 192.168.20.5 80
```

Check connection to a network

```
netstat -rn
netstat -putan 192.168.20.5 |more   //q for exit
```

## Host name

List all hosts (change in ***/etc/init.d/hotname.sh*** require reboot system)

```
cat /etc/hostname
```

```
hostname    //Display current hostname
```