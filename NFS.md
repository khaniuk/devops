# NFS (Network File System)

## Install NFS

Debian: Update repository and install on Server Machine
```
sudo apt-get update

sudo apt install nfs-kernel-server
```

## Create the Export Directory

Run the following command by mentioning the export directory path as follows
```
sudo mkdir -p /mnt/sharedfolder
```

To allow all clients access permission to the export directory, needed to remove restrictive permissions.
```
sudo chown nobody:nogroup /mnt/sharedfolder
```

Then apply new permission that allows everyone read, write and execute access.
```
sudo chmod 755 /mnt/sharedfolder
```

## Configure the export directory

Open file with some text editor
```
sudo nano /etc/exports
```

In order to allow access to clients, add the following line in it ***directory hostname(options)***
```
/mnt/sharedfolder client1_IP(rw,sync,no_subtree_check)

/mnt/sharedfolder client2_IP(rw,sync,no_subtree_check)
```

The parameters (rw,sync,no_subtree_check) in above file means the client has the following permissions
* **rw**: read and write operations
* **sync**: write any change to the disc before applying it
* **no_subtree_check**: no subtree checking

In order to allow access to multiple clients by specifying a whole subnet, add the following line in it
```
/mnt/sharedfolder subnet_IP/24(rw,sync,no_subtree_check)
```

## Export the shared directory

Export the shared directory listed in /etc/exports and restart NFS server
```
sudo exportfs -a

sudo systemctl restart nfs-kernel-server
```

## Configure firewall

Add the rule that allows traffic from the specified clients to the NFS port
```
sudo ufw allow from [client_IP or client_Subnet] to any port nfs
```

Verify if the rule is successfully added
```
sudo ufw status
```

# Configuring the Client Machine

## Install NFS client

First, update your client machine repository and install
```
sudo apt-get update

sudo apt-get install nfs-common
```

## Create a mount point for the NFS sever’s shared folder

Creating the mount point with the name "sharedfolder_client" in the /mnt directory
```
sudo mkdir -p /mnt/sharedfolder_client
```

## Mount the server’s shared directory on the client

Mount the NFS server’s shared directory to the above-created mount point
```
sudo mount server_IP:/exportFolder_server /mnt/mountfolder_client
```

For permanent mount add the following entry in /etc/fstab file
```
sudo vi /etc/fstab
```

Add following line at the end of the file
```
server_IP:/exportFolder_server  /mnt/mountfolder_client nfs4 defaults,user,exec 0 0
```

Where
* ***server_IP:/mnt/nfsshare***: shared folder coming from nfs server
* ***/mnt/shared_nfs***: mount directory in client machine
* ***nfs4***: signifies nfs version 4
* ***defaults,user,exec***: Permit any user to mount the file system also allow them to exec binaries

Mount the NFS file system
```
sudo mount -a
```
